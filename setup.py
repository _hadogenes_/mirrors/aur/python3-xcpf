#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='''XCPF''',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1640341361)),
  description='''Xyne's common Pacman functions, for internal use.''',
  author='''Xyne''',
  author_email='''gro xunilhcra enyx, backwards''',
  url='''http://xyne.dev/projects/python3-xcpf''',
  packages=['''XCPF'''],
)
